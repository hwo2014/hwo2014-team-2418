
package gameinit;

import com.google.gson.annotations.Expose;

public class Car {

    @Expose
    private Id id;
    @Expose
    private Dimensions dimensions;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }

}
