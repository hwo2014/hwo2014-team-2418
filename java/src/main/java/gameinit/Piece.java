
package gameinit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Piece {

    @Expose
    private Float length;
    @SerializedName("switch")
    @Expose
    private Boolean _switch;
    @Expose
    private Integer radius;
    @Expose
    private Float angle;

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Float getAngle() {
        return angle;
    }

    public void setAngle(Float angle) {
        this.angle = angle;
    }

}
