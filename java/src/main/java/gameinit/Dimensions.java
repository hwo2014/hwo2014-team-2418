
package gameinit;

import com.google.gson.annotations.Expose;

public class Dimensions {

    @Expose
    private Float length;
    @Expose
    private Float width;
    @Expose
    private Float guideFlagPosition;

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getGuideFlagPosition() {
        return guideFlagPosition;
    }

    public void setGuideFlagPosition(Float guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }

}
