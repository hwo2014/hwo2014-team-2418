
package gameinit;

import com.google.gson.annotations.Expose;

public class GameInit {

    @Expose
    private String msgType;
    @Expose
    private Data data;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
