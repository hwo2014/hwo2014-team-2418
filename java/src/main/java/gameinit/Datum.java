
package gameinit;

import carposition.PiecePosition;
import com.google.gson.annotations.Expose;

public class Datum {

    @Expose
    private Id id;
    @Expose
    private Float angle;
    @Expose
    private PiecePosition piecePosition;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Float getAngle() {
        return angle;
    }

    public void setAngle(Float angle) {
        this.angle = angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

}
