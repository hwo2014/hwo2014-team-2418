
package gameinit;

import com.google.gson.annotations.Expose;

public class Lane {

    @Expose
    private Integer distanceFromCenter;
    @Expose
    private Integer index;

    public Integer getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(Integer distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

}
