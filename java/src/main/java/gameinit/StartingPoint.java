
package gameinit;

import com.google.gson.annotations.Expose;

public class StartingPoint {

    @Expose
    private Position position;
    @Expose
    private Float angle;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Float getAngle() {
        return angle;
    }

    public void setAngle(Float angle) {
        this.angle = angle;
    }

}
