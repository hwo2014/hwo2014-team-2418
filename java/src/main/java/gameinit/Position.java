
package gameinit;

import com.google.gson.annotations.Expose;

public class Position {

    @Expose
    private Float x;
    @Expose
    private Float y;

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

}
