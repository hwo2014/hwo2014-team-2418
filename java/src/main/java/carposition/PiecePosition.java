
package carposition;

import com.google.gson.annotations.Expose;

public class PiecePosition {

    @Expose
    private Integer pieceIndex;
    @Expose
    private Float inPieceDistance;
    @Expose
    private Lane lane;
    @Expose
    private Integer lap;

    public Integer getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(Integer pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public Float getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(Float inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public Lane getLane() {
        return lane;
    }

    public void setLane(Lane lane) {
        this.lane = lane;
    }

    public Integer getLap() {
        return lap;
    }

    public void setLap(Integer lap) {
        this.lap = lap;
    }

}
