
package carposition;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class CarPosition {

    @Expose
    private String msgType;
    @Expose
    private List<Datum> data = new ArrayList<Datum>();
    @Expose
    private String gameId;
    @Expose
    private Integer gameTick;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Integer getGameTick() {
        return gameTick;
    }

    public void setGameTick(Integer gameTick) {
        this.gameTick = gameTick;
    }

}
