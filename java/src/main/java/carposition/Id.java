
package carposition;

import com.google.gson.annotations.Expose;

public class Id {

    @Expose
    private String name;
    @Expose
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
