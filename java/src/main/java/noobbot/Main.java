package noobbot;

import carposition.CarPosition;
import carposition.Datum;
import com.google.gson.Gson;
import createrace.BotId;
import createrace.CreateRace;
import createrace.Data;
import gameinit.GameInit;
import gameinit.Piece;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private GameInit gameInit;
    private DrivingUtils drivingUtils = new DrivingUtils();
    private List<Integer> switchLanes = new LinkedList<>();

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        //send(join);
        createCustomRace(join.key, join.name);

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            switch (msgFromServer.msgType) {
                case "carPositions":
                    Datum carData = drivingUtils.getCarData(gson.fromJson(line, CarPosition.class).getData(), join.name);
                    if (carData.getPiecePosition().getPieceIndex() == 0 && carData.getPiecePosition().getLap() > 0) {
                        switchLanes.clear();
                    }
                    List<Piece> trackPieces = gameInit.getData().getRace().getTrack().getPieces();
                    if (carData.getPiecePosition().getPieceIndex() < trackPieces.size() - 2 && 
                            trackPieces.get(carData.getPiecePosition().getPieceIndex() + 1).getSwitch() != null && 
                            trackPieces.get(carData.getPiecePosition().getPieceIndex() + 1).getSwitch() == true &&
                            !switchLanes.contains(carData.getPiecePosition().getPieceIndex() + 1) && 
                            trackPieces.get(carData.getPiecePosition().getPieceIndex() + 2).getAngle() != null) {
                        switchLanes.add(carData.getPiecePosition().getPieceIndex() + 1);
                        send(new SwitchLanes(drivingUtils.getLane(trackPieces.get(carData.getPiecePosition().getPieceIndex() + 2))));
                    } else {
                        send(new Throttle(handleCarPosition(carData, trackPieces)));
                    }
                    break;
                case "join":
                    System.out.println("Joined");
                    send(new Ping());
                    break;
                case "gameInit":
                    System.out.println("Race init");
                    gameInit = gson.fromJson(line, GameInit.class);
                    System.out.println(line);
                    send(new Ping());
                    break;
                case "gameEnd":
                    System.out.println("Race end");
                    send(new Ping());
                    break;
                case "gameStart":
                    System.out.println("Race start");
                    send(new Throttle(1.0));
                    break;
                default:
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    private void createCustomRace(String key, String name) {
        //CreateRace createRace = new CreateRace();
        //createRace.setMsgType("createRace");
        Data data = new Data();
        data.setCarCount(1);
        data.setTrackName("germany");
        data.setPassword(key);
        BotId botId = new BotId();
        botId.setKey(key);
        botId.setName(name);
        data.setBotId(botId);
        //createRace.setData(data);
        //sendMsg(createRace);

        CreateRace joinRace = new CreateRace();
        joinRace.setMsgType("joinRace");
        joinRace.setData(data);
        sendMsg(joinRace);
    }

    private void sendMsg(Object msg) {
        writer.println(new Gson().toJson(msg));
        writer.flush();
    }

    private double handleCarPosition(Datum carData, List<Piece> trackPieces) {
        if (carData != null || trackPieces != null) {
            return drivingUtils.getThrottle(trackPieces, carData);
        }
        return 0.5;
    }
}

abstract class SendMsg {

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {

    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {

    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {

    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {

    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLanes extends SendMsg {

    private String value;

    public SwitchLanes(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class Turbo extends SendMsg {

    private String value;

    public Turbo(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
