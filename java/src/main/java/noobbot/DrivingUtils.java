package noobbot;

import carposition.Datum;
import gameinit.Piece;
import java.util.List;

public class DrivingUtils {
    
    private final double carAngleTest = 10.0, zeroDouble = 0.0, fullSpeed = 1.0;
    public static double currentSpeed;

    public DrivingUtils() {

    }

    public double getThrottle(List<Piece> trackPieces, Datum carData) {
        if (trackPieces == null || carData == null) {
            return currentSpeed = fullSpeed;
        }
        int pieceIndex = carData.getPiecePosition().getPieceIndex();
        if (pieceIndex < 0 || pieceIndex >= trackPieces.size()) {
            return currentSpeed = fullSpeed;
        }
        if (pieceIndex == trackPieces.size() - 1) {
            return currentSpeed = fullSpeed;
        }

        Piece currentPiece = trackPieces.get(carData.getPiecePosition().getPieceIndex()),
                nextPiece = trackPieces.get(carData.getPiecePosition().getPieceIndex() + 1);

        double carAngle;
        
        if (carData.getAngle() == null) {
            carAngle = zeroDouble;
        } else {
            if (Double.compare(carData.getAngle().doubleValue(), zeroDouble) < 0) {
                carAngle = carData.getAngle().doubleValue() * -1.0;
            } else {
                carAngle = carData.getAngle().doubleValue();
            }
        }
        
        if (currentPiece.getAngle() == null && nextPiece.getAngle() == null) {
            if (Double.compare(carAngle, carAngleTest) > 0) {
                return currentSpeed = zeroDouble;
            } 
            return currentSpeed = fullSpeed;
        }

        double currentPieceAngle = zeroDouble, nextPieceAngle = zeroDouble;
        int currentPieceRadius = 0, nextPieceRadius = 0;
        if (currentPiece.getAngle() != null && currentPiece.getRadius() != null) {
            if (Double.compare(currentPiece.getAngle().doubleValue(), zeroDouble) < 0) {
                currentPieceAngle = currentPiece.getAngle().doubleValue() * -1.0;
            } else {
                currentPieceAngle = currentPiece.getAngle().doubleValue();
            }
            currentPieceRadius = currentPiece.getRadius();
        }
        if (nextPiece.getAngle() != null && nextPiece.getRadius() != null) {
            if (Double.compare(nextPiece.getAngle().doubleValue(), zeroDouble) < 0) {
                nextPieceAngle = nextPiece.getAngle().doubleValue() * -1.0;
            } else {
                nextPieceAngle = nextPiece.getAngle().doubleValue();
            }
            nextPieceRadius = nextPiece.getRadius();
        }
        double testAngle = 45.0;
        int testRadius = 100;

        if (currentPiece.getAngle() == null && nextPiece.getAngle() != null) {
            if (carData.getPiecePosition().getPieceIndex() - 1 >= 0) {
                Piece previousPiece = trackPieces.get(carData.getPiecePosition().getPieceIndex() - 1);
                if (previousPiece.getAngle() != null && previousPiece.getRadius() != null) {
                    return currentSpeed = countThrottle(nextPieceAngle, nextPieceRadius, carAngle);
                }
            }

            if (Double.compare(nextPieceAngle, testAngle) >= 0 && nextPieceRadius <= testRadius && 
                    Double.compare(currentSpeed, zeroDouble) != 0) {
                return currentSpeed = zeroDouble;
            }
            return currentSpeed = countThrottle(nextPieceAngle, nextPieceRadius, carAngle);
        }
        if (currentPiece.getAngle() != null && nextPiece.getAngle() == null) {
            return currentSpeed = countThrottle(currentPieceAngle, currentPieceRadius, carAngle);
        }
        if (currentPiece.getAngle() != null && nextPiece.getAngle() != null) {
            return currentSpeed = countThrottle(currentPieceAngle, currentPieceRadius, carAngle);
        }
        return currentSpeed = fullSpeed;
    }

    private double countThrottle(double angle, int radius, double carAngle) {
        double trackAngleSpeed = 1.0 - (angle / radius);
        if (Double.compare(carAngle, zeroDouble) == 0) {
            return trackAngleSpeed;
        }
        if (Double.compare(carAngle, carAngleTest) > 0 && Double.compare(currentSpeed, zeroDouble) != 0) {
            return zeroDouble;
        }
        if (Double.compare(currentSpeed + 0.1, fullSpeed) < 0) {
            return currentSpeed + 0.1;
        }
        return trackAngleSpeed;
    }

    public String getLane(Piece nextTurn) {
        if (Double.compare(nextTurn.getAngle().doubleValue(), zeroDouble) < 0) {
            return "Left";
        } else {
            return "Right";
        }
    }

    public Datum getCarData(List<Datum> data, String carName) {
        if (data == null || carName == null) {
            return null;
        }
        for (Datum car : data) {
            if (car.getId().getName().equalsIgnoreCase(carName)) {
                return car;
            }
        }
        return null;
    }

}
