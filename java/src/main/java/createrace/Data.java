
package createrace;

import com.google.gson.annotations.Expose;

public class Data {

    @Expose
    private BotId botId;
    @Expose
    private String trackName;
    @Expose
    private String password;
    @Expose
    private Integer carCount;

    public BotId getBotId() {
        return botId;
    }

    public void setBotId(BotId botId) {
        this.botId = botId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getCarCount() {
        return carCount;
    }

    public void setCarCount(Integer carCount) {
        this.carCount = carCount;
    }

}
