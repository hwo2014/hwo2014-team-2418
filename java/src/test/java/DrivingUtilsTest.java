
import carposition.Datum;
import carposition.PiecePosition;
import gameinit.Piece;
import java.util.ArrayList;
import java.util.List;
import noobbot.DrivingUtils;

public class DrivingUtilsTest {

    public DrivingUtilsTest() {
    }

    public void testThrottle() {
        List<Piece> trackPieces = new ArrayList<>();
        Datum carData = new Datum();
        carData.setPiecePosition(new PiecePosition());
        
        Piece straight = new Piece(), gentleTurnLeft = new Piece(), sharpTurnLeft = new Piece(),
                gentleTurnRight = new Piece(), sharpTurnRight = new Piece();
        
        straight.setLength(new Float(100));
        straight.setSwitch(Boolean.TRUE);
        gentleTurnLeft.setAngle(new Float(-22.5));
        gentleTurnLeft.setRadius(200);
        gentleTurnRight.setAngle(new Float(22.5));
        gentleTurnRight.setRadius(200);
        gentleTurnRight.setSwitch(Boolean.TRUE);
        sharpTurnLeft.setAngle(new Float(-45));
        sharpTurnLeft.setRadius(100);
        sharpTurnLeft.setSwitch(Boolean.TRUE);
        sharpTurnRight.setAngle(new Float(45));
        sharpTurnRight.setRadius(100);
        
        trackPieces.add(0, straight);
        trackPieces.add(1, straight);
        trackPieces.add(2, straight);
        trackPieces.add(3, straight);
        trackPieces.add(4, sharpTurnRight);
        trackPieces.add(5, sharpTurnRight);
        trackPieces.add(6, sharpTurnRight);
        trackPieces.add(7, sharpTurnRight);
        trackPieces.add(8, gentleTurnRight);
        trackPieces.add(9, straight);
        trackPieces.add(10, straight);
        trackPieces.add(11, gentleTurnLeft);
        trackPieces.add(12, straight);
        trackPieces.add(13, straight);
        trackPieces.add(14, sharpTurnLeft);
        trackPieces.add(15, sharpTurnLeft);
        trackPieces.add(16, sharpTurnLeft);
        trackPieces.add(17, sharpTurnLeft);
        trackPieces.add(18, straight);
        trackPieces.add(19, sharpTurnRight);
        trackPieces.add(20, sharpTurnRight);
        trackPieces.add(21, sharpTurnRight);
        trackPieces.add(22, sharpTurnRight);
        trackPieces.add(23, gentleTurnRight);
        trackPieces.add(24, gentleTurnLeft);
        trackPieces.add(25, straight);
        trackPieces.add(26, sharpTurnRight);
        trackPieces.add(27, sharpTurnRight);
        trackPieces.add(28, straight);
        trackPieces.add(29, sharpTurnLeft);
        trackPieces.add(30, sharpTurnLeft);
        trackPieces.add(31, sharpTurnRight);
        trackPieces.add(32, sharpTurnRight);
        trackPieces.add(33, sharpTurnRight);
        trackPieces.add(34, sharpTurnRight);
        trackPieces.add(35, straight);
        trackPieces.add(36, straight);
        trackPieces.add(37, straight);
        trackPieces.add(38, straight);
        trackPieces.add(39, straight);
        
        DrivingUtils utils = new DrivingUtils();
        for (int i = 0; i < trackPieces.size(); i++) {
            carData.getPiecePosition().setPieceIndex(i);
            System.out.println(i + ": " + utils.getThrottle(trackPieces, carData));
        }
    }
}
